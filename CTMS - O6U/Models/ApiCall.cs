﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;


namespace CTMS___O6U.Models
{
    public class ApiCall
    {
        static HttpClient client;

        static ApiCall()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:53002");
        }

        public static HttpResponseMessage getFromApi(string apiLink)
        {

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage res = client.GetAsync(apiLink).Result;

            if (res.IsSuccessStatusCode)
            {
                return res;
            }

            return null;
        }

        public static int getFromApi(string apiLink, object item)
        {

            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            try
            {

                //client.PostAsJsonAsync(apiLink, item).ContinueWith(postTask => postTask.Result.EnsureSuccessStatusCode());
                var response = client.PostAsJsonAsync(apiLink, item).Result;
                int token = (int) response.StatusCode;
           
                return token;
                
            }
            catch (HttpRequestException)
            {
                return 0;
            }

        }

        public static int putFromApi(string apiLink,int id,object item)
        {
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            try
            {

                var response = client.PutAsJsonAsync(apiLink,new { id = id ,car = item}).Result;
                int token = (int)response.StatusCode;

                return token;
            }
            catch (HttpRequestException)
            {
                return 0;
            }
        }

        public static int deleteFromApi(string apiLink)
        {
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            try
            {

                var response = client.DeleteAsync(apiLink).Result;
                int token = (int)response.StatusCode;

                return token;
            }
            catch (HttpRequestException)
            {
                return 0;
            }
        }
    }
}