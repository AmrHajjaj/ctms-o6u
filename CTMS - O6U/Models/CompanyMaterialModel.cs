﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CTMS___O6U.Models
{
    public class CompanyMaterialModel
    {
        [Required]
        public int id { get; set; }

        [Required]
        [Display(Name = "Company")]
        public int company_id { get; set; }

        [Required]
        [Display(Name = "Material Name")]
        public int matrial_id { get; set; }

        [Required]
        public bool Archive { get; set; }

        public virtual CompanyModel company { get; set; }
    }
}