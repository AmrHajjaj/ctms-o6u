﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTMS___O6U.Models
{
    public class TransportedMatrialModel
    {
        public TransportedMatrialModel()
        {
            this.car_transport_driver = new HashSet<CarTransportedDriverModel>();
        }

        public int id { get; set; }
        
        //dummy
        public string source { get; set; }

        //dummy
        public string destination { get; set; }

        public System.DateTime time_stamp { get; set; }
        public int Quantity { get; set; }

        public bool Archive { get; set; }
        public int material_id { get; set; }

        //public int material_id { get; set; }


        public virtual ICollection<CarTransportedDriverModel> car_transport_driver { get; set; }
    }
}