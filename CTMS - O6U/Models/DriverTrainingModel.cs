﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CTMS___O6U.Models
{
    public class DriverTrainingModel
    {

        [Display(Name = "Driver Name")]
        public int Driver_id { get; set; }

        [Display(Name = "Training Name")]
        public int Training_id { get; set; }

        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public System.DateTime StartingDate { get; set; }

        [Required]
        public bool Archive { get; set; }
        public int id { get; set; }
        

        [Required]
        [StringLength(150)]
        public string Notes { get; set; }


        public virtual DriverModel driver { get; set; }
        public virtual TrainingModel Training { get; set; }
    }
}