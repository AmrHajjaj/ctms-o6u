﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CTMS___O6U.Models.Utils;
namespace CTMS___O6U.Models
{
    public class DriverModel
    {
        
        public int id { get; set; }

        [Display(Name ="National Id")]
        [Required]
        [RegularExpression(ValidationsUtil.REGEX_SSN , ErrorMessage = ValidationsUtil.MSG_SSN)]
        public string ssn { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [RegularExpression(ValidationsUtil.REGEX_PHONE_NUMBER , ErrorMessage = ValidationsUtil.MSG_PHONE_NUMBER)]
        public string phone { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }

        [Required]
        [Display(Name = "Birthdate")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public System.DateTime birth_date { get; set; }

        [Required]
        [Display(Name = "License Degree")]
        [StringLength(1)]
        public string license_degree { get; set; }

        //[Required]
        [Display(Name = "Company Name")]
        public int companys_id { get; set; }

        [Required]
        public bool Archive { get; set; }

        public virtual CompanyModel company { get; set; }
        public virtual ICollection<DriverTrainingModel> Drivers_Training { get; set; }
        public virtual ICollection<CarTransportedDriverModel> car_transport_driver { get; set; }

    }
}