﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CTMS___O6U.Models.Utils
{
    public class ValidationsUtil
    {
        public const string MSG_POSTIVE_NUMBER = "Only number Greater than Zero allowed";
        public const int MIN_POSTIVE_NUMBER = 1;
        public const int MAX_POSTIVE_NUMBER = int.MaxValue;

        public const string MSG_PHONE_NUMBER = "Invaild Mobile Number Format";
        public const string REGEX_PHONE_NUMBER = "(01)[0-9]{9}";

        public const string MSG_SSN = "Invaild National Id Format";
        public const string REGEX_SSN = "(2|3)[0-9]{13}";

        public const string MSG_FAX_NUMBER = "Invaild Fax Number Format";
        public const string REGEX_FAX_NUMBER = "(0)[0-9]{1,2}[0-9]{8}";

    }
}