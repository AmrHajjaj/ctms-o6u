﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CTMS___O6U.Models
{
    public class TrainingModel
    {

        public TrainingModel()
        {
            this.Drivers_Training = new HashSet<DriverTrainingModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }


        [Required]
        public bool Archive { get; set; }

        [Display(Name="Duration(Months)")]
        [Range(1, int.MaxValue, ErrorMessage = Utils.ValidationsUtil.MSG_POSTIVE_NUMBER)]
        public int Duration { get; set; }
        public virtual ICollection<DriverTrainingModel> Drivers_Training { get; set; }

    }
}