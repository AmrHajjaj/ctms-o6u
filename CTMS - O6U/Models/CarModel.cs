﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CTMS___O6U.Models.Utils;

namespace CTMS___O6U.Models
{

    public class CarModel
    {

        public int id { get; set; }

        [Required]
        [Display(Name = "Plate Number")]
        [StringLength(8)]
        public string plate_number { get; set; }

        [Required]
        [Display(Name = "Color")]
        public string color { get; set; }


        [Required]
        [Display(Name = "Capcity")]
        [Range(ValidationsUtil.MIN_POSTIVE_NUMBER, ValidationsUtil.MAX_POSTIVE_NUMBER, ErrorMessage = Utils.ValidationsUtil.MSG_POSTIVE_NUMBER)]
        public int Capcity { get; set; }

        [Required]
        [Display(Name = "Width")]
        [Range(ValidationsUtil.MIN_POSTIVE_NUMBER, ValidationsUtil.MAX_POSTIVE_NUMBER, ErrorMessage = Utils.ValidationsUtil.MSG_POSTIVE_NUMBER)]
        public int width { get; set; }

        
        [Required]
        [Display(Name = "Length")]
        [Range(ValidationsUtil.MIN_POSTIVE_NUMBER, ValidationsUtil.MAX_POSTIVE_NUMBER, ErrorMessage = Utils.ValidationsUtil.MSG_POSTIVE_NUMBER)]
        public int length { get; set; }

        [Required]
        [Display(Name = "Weight")]
        [Range(ValidationsUtil.MIN_POSTIVE_NUMBER, ValidationsUtil.MAX_POSTIVE_NUMBER, ErrorMessage = Utils.ValidationsUtil.MSG_POSTIVE_NUMBER)]
        public int weight { get; set; }

        [Display(Name = "Satisfied Critera?")]
        public bool satisfied { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string model { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        public int company_id { get; set; }
        public bool Archive { get; set; }

        public virtual ICollection<CarTransportedDriverModel> car_transport_driver { get; set; }
        public virtual CompanyModel company { get; set; }
    }
}