﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CTMS___O6U.Models.Utils;

namespace CTMS___O6U.Models
{
    public class CompanyModel
    {
        public int id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "e-mail")]
        [EmailAddress]
        public string mail { get; set; }

        [Required]
        [Display(Name = "Address")]
        public string address { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        [RegularExpression(ValidationsUtil.REGEX_PHONE_NUMBER,ErrorMessage = ValidationsUtil.MSG_PHONE_NUMBER)]
        public string phone { get; set; }

        [Required]
        [Display(Name = "Fax Number")]
        [RegularExpression(ValidationsUtil.REGEX_FAX_NUMBER , ErrorMessage = ValidationsUtil.MSG_FAX_NUMBER)]
        public string fax { get; set; }

        [Required]
        [Display(Name = "Postal Code")]
        public string postal_code { get; set; }


        public bool Archive { get; set; }

        public ICollection<CarModel> cars { get; set; }
        public virtual ICollection<DriverModel> drivers { get; set; }
        public virtual ICollection<CompanyMaterialModel> Comapny_Materials { get; set; }

    }
}