﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CTMS___O6U.Models
{
    public class CarTransportedDriverModel
    {
        public int id { get; set; }
        public int car_id { get; set; }
        public int transported_id { get; set; }
        public int driver_id { get; set; }

        [Required]
        public bool Archive { get; set; }


        public virtual CarModel car { get; set; }
        public virtual TransportedMatrialModel transported_matriels { get; set; }
        public virtual DriverModel driver { get; set; }
    }
}