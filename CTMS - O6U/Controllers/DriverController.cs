﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CTMS___O6U.Models;
using System.Net.Http;
using System.Web.UI;

namespace CTMS___O6U.Controllers
{
    public class DriverController : Controller
    {
        // GET: Driver
        public ActionResult Index()
        {
            try
            {
                ViewBag.RequestResult = TempData["RequestResult"].ToString();
            }
            catch (Exception) { }

            List<DriverModel> driver = new List<DriverModel>();

            HttpResponseMessage res = ApiCall.getFromApi("api/drivers");
            DriverModel [] dtt = null;
            if (res != null)
            {
               var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<DriverModel []>(d);
                var q = dtt.Where(e => e.Archive == false);
                return View(q);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Index(string statue)
        {
            TempData["RequestResult"] = statue;
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "id,ssn,phone,name,address,birth_date,license_degree,companys_id")] DriverModel driver)
        {
            driver.Archive = false;
            int res = ApiCall.getFromApi("api/drivers/", driver);
            switch (res)
            {
                case 201:
                    return Index("Record Added Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Delete(int id)
        {

            int res = ApiCall.deleteFromApi("api/drivers/" + id);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted!");
                default:
                    break;
            }

            return Index("Something went worng");
        }

        public ActionResult Edit(int id)
        {
            HttpResponseMessage driverdata = ApiCall.getFromApi("api/drivers/" + id);
            if (driverdata == null)
                return HttpNotFound();

            List<DriverModel> drivers = new List<DriverModel>();
            drivers.Add(driverdata.Content.ReadAsAsync<DriverModel>().Result);
            DriverModel driver = drivers[0];
            return View(driver);
        }

        public ActionResult Save(int id, DriverModel driver)
        {
            int res = ApiCall.getFromApi("api/drivers/" + id, driver);
            switch (res)
            {
                case 201:
                    return Index("Record Saved Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }


        public ActionResult Details(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/Drivers/" + id);
            DriverModel driver = new DriverModel();
            if (res != null)
            {
                driver = res.Content.ReadAsAsync<DriverModel>().Result;
            }

            return View(driver);
        }


    }
}