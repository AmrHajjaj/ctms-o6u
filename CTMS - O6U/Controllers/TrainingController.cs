﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CTMS___O6U.Models;
using System.Net.Http;
using System.Text.RegularExpressions;

namespace CTMS___O6U.Controllers
{
    public class TrainingController : Controller
    {
        // GET: Training
        public ActionResult Index()
        {
            try
            {
                ViewBag.RequestResult = TempData["RequestResult"].ToString();
            }
            catch (Exception) { }

            List<TrainingModel> training = new List<TrainingModel>();

            HttpResponseMessage res = ApiCall.getFromApi("api/trainings");
            TrainingModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<TrainingModel[]>(d);
                var ret = dtt.Where(e => e.Archive == false);
                return View(ret);
            }

            return View();
        }

        [HttpPost]
        public ActionResult Index(string statue)
        {
            TempData["RequestResult"] = statue;
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            HttpResponseMessage trainingdata = ApiCall.getFromApi("api/trainings/" + id);
            if (trainingdata == null)
                return HttpNotFound();

            List<TrainingModel> tainings = new List<TrainingModel>();
            tainings.Add(trainingdata.Content.ReadAsAsync<TrainingModel>().Result);
            TrainingModel taining = tainings[0];
            return View(taining);
        }


        public ActionResult Save(int id, TrainingModel training)
        {
            int res = ApiCall.getFromApi("api/trainings/" + id, training);
            switch (res)
            {
                case 201:
                    return Index("Record Saved Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "Id,Name,Duration")] TrainingModel training)
        {
            training.Archive = false;
            int res = ApiCall.getFromApi("api/trainings/", training);
            switch (res)
            {
                case 201:
                    return Index("Record Added Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }


        public ActionResult Details(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/Trainings/" + id);
            TrainingModel taining = new TrainingModel();
            if (res != null)
            {
                taining = res.Content.ReadAsAsync<TrainingModel>().Result;
            }

            return View(taining);
        }

        public ActionResult Delete(int id)
        {

            int res = ApiCall.deleteFromApi("api/trainings/" + id);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted!");
                default:
                    break;
            }

            return Index("Something went worng");
        }

        public ActionResult Assign(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/companies");
            CompanyModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<CompanyModel[]>(d);
            }

            ViewBag.CompanyNames = new SelectList(dtt.Where(e => e.Archive == false), "id", "name");

            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult Assign(string dataName) {
            try
            {
                string[] dataVal = dataName.Split('~');
                //int[] myInts = Array.ConvertAll(dataVal, s => int.Parse(s));
                for (int i = 0; i < dataVal.Length; i++)
                {
                    DriverTrainingModel driverTraining = new DriverTrainingModel();
                    driverTraining.Driver_id = int.Parse(dataVal[i]);
                    driverTraining.Notes = dataVal[++i];
                    driverTraining.Training_id = Convert.ToInt32(RouteData.Values["id"]);
                    DateTime curr = DateTime.Now;
                    driverTraining.StartingDate = Convert.ToDateTime(curr.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    int res = ApiCall.getFromApi("api/DriversTraining/", driverTraining);
                }

                return Index("Drivers Assigned");
            }
            catch (HttpException e)
            {
                return Index("Something went worng");
            }
        }

        public JsonResult getDrivers(int companyId)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/drivers/bycompany/" + companyId);
            DriverModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<DriverModel[]>(d);
            }

            return Json(dtt.Where(e => e.Archive == false), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Unassign(int id)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult Unassign([Bind(Include = "Driver_id")] string driver_id)
        {
            string trinaingId = Url.RequestContext.RouteData.Values["id"].ToString(); 
            int res = ApiCall.deleteFromApi("api/DriversTraining?ids="+ driver_id 
                + "&ids=" + trinaingId);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted !");
                default:
                    break;
            }

            return Index("Something went worng");
        }

    }
}