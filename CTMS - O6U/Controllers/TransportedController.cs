﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CTMS___O6U.Models;
using System.Net.Http;
using System.Text.RegularExpressions;
using Microsoft.Reporting.WebForms;
using System.IO;

namespace CTMS___O6U.Controllers
{
    public class TransportedController : Controller
    {
        // GET: Transported
        public ActionResult Index()
        {
            try
            {
                ViewBag.RequestResult = TempData["RequestResult"].ToString();
            }
            catch (Exception) { }

            List<TransportedMatrialModel> training = new List<TransportedMatrialModel>();

            HttpResponseMessage res = ApiCall.getFromApi("api/TransportedMatriels");
            TransportedMatrialModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<TransportedMatrialModel[]>(d);
            }
            return View(dtt.Where(e => e.Archive == false));
            //return View();
        }

        [HttpPost]
        public ActionResult Index(string statue)
        {

            TempData["RequestResult"] = statue;
            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        { 
            HttpResponseMessage transportedgData = ApiCall.getFromApi("api/TransportedMatriels/" + id);
            if (transportedgData == null)
                return HttpNotFound();

            List<TransportedMatrialModel> data = new List<TransportedMatrialModel>();
            data.Add(transportedgData.Content.ReadAsAsync<TransportedMatrialModel>().Result);
            TransportedMatrialModel tdata = data[0];
            return View(tdata);
        }

        public ActionResult Save(int id, TransportedMatrialModel transported)
        {
            int res = ApiCall.getFromApi("api/TransportedMatriels/" + id, transported);
            switch (res)
            {
                case 201:
                    return Index("Record Saved Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Create()
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/DummyMaterials");
            DummyMaterialModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<DummyMaterialModel[]>(d);
            }

            ViewBag.materials = new SelectList(dtt, "matrial_id", "name");

            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "id,source,destination,material_id,quantity")] TransportedMatrialModel transported)
        {
            DateTime curr = DateTime.Now;
            transported.time_stamp = Convert.ToDateTime(curr.ToString("yyyy-MM-dd HH:mm:ss.fff"));
            int res = ApiCall.getFromApi("api/TransportedMatriels", transported);
            switch (res)
            {
                case 201:
                    return Index("Record Added Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Details(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/TransportedMatriels/" + id);
            TransportedMatrialModel transportedMaterials = new TransportedMatrialModel();
            if (res != null)
            {
                transportedMaterials = res.Content.ReadAsAsync<TransportedMatrialModel>().Result;
            }

            return View(transportedMaterials);
        }

        public ActionResult Delete(int id)
        {

            int res = ApiCall.deleteFromApi("api/TransportedMatriels/" + id);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted!");
                default:
                    break;
            }

            return Index("Something went worng");
        }

        [HttpGet]
        public ActionResult Assign(int id)
        {
            HttpResponseMessage matres = ApiCall.getFromApi("api/TransportedMatriels/"+id);
            TransportedMatrialModel tranMat = null;
            if(matres != null)
            {
                var d = matres.Content.ReadAsStringAsync().Result;
                tranMat = Newtonsoft.Json.JsonConvert.DeserializeObject<TransportedMatrialModel>(d);
            }



            HttpResponseMessage res = ApiCall.getFromApi("api/ComapnyMaterials");
            CompanyMaterialModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<CompanyMaterialModel[]>(d);
            }

            ViewBag.CompanyNames = new SelectList(dtt.Where(e => e.Archive == false)
                .Where(e => e.matrial_id == tranMat.material_id).Select(e => e.company), "id","name");

            ViewBag.id = id;

            ViewBag.quantity = tranMat.Quantity;
            return View();
        }

        [HttpPost]
        public ActionResult Assign(string dataName)
        {
            try
            {
                string cleanedData = Regex.Replace(dataName, @"[^0-9a-zA-Z,]", "");
                string[] dataVal = cleanedData.Split(',');
                int[] myInts = Array.ConvertAll(dataVal, s => int.Parse(s));
                for (int i = 0; i < myInts.Length; i++)
                {
                    CarTransportedDriverModel addData = new CarTransportedDriverModel();
                    addData.car_id = myInts[i];
                    addData.transported_id = Convert.ToInt32(RouteData.Values["id"]);
                    addData.driver_id = myInts[++i];

                    int res = ApiCall.getFromApi("api/CarTransportDriver", addData);

                }
                return Index("Driver Assigned");
            }
            catch(HttpException e)
            {
                return Index("Something went worng");
            }
            
        }

        public JsonResult getDrivers(int companyId)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/drivers/bycompany/"+companyId);
            DriverModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<DriverModel[]>(d);
            }

            return Json(dtt.Where(e => e.Archive == false), JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCars(int companyId)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/cars/bycompany/"+companyId);
            CarModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<CarModel[]>(d);
            }

            return Json(dtt.Where(e => e.Archive == false && e.satisfied == true), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Unassign(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/CarTransportDriver");
            CarTransportedDriverModel[] dtt = null;
            if (res != null)
            {
                var d = res.Content.ReadAsStringAsync().Result;
                dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<CarTransportedDriverModel[]>(d);
            }


            ViewBag.id = id;
            return View(dtt.Where(e => e.Archive == false));
        }

        [HttpPost]
        public ActionResult Unassign(string dataName)
        {
            try
            {
                string cleanedData = Regex.Replace(dataName, @"[^0-9a-zA-Z,]", "");
                string[] dataVal = cleanedData.Split(',');
                int[] myInts = Array.ConvertAll(dataVal, s => int.Parse(s));
                for (int i = 0; i < dataVal.Length; i++)
                {
                    int id = myInts[i];

                    int res = ApiCall.deleteFromApi("api/CarTransportDriver/" + id);
                    switch (res)
                    {
                        case 200:
                            return Index("Record Deleted!");
                        default:
                            break;
                    }

                    return Index("Something went worng");

                }
                return Index("Driver Assigned");
            }
            catch (HttpException e)
            {
                return Index("Something went worng");
            }

        }

        public ActionResult Report(DateTime? startDate, DateTime? endDate/*,int? company_id*/)
        {

            List<TransportedMatrialModel> company = new List<TransportedMatrialModel>();
            HttpResponseMessage res = ApiCall.getFromApi("api/TransportedMatriels");

            if (res != null)
            {
                company = res.Content.ReadAsAsync<List<TransportedMatrialModel>>().Result;
                var ret = company.Where(e => e.Archive == false);

                if (startDate != null && endDate != null)
                {
                    ret = ret.Where(e => e.time_stamp.Date >= startDate.Value.Date && e.time_stamp.Date <= endDate.Value.Date);
                }
                //if (company_id != null)
                //    ret.Where(e => e.car_transport_driver.Where(c=> c.car.company_id == company_id));
                return View(ret);
            }

            return View();
        }

        //public ActionResult Report(string ReportType)
        //{
        //    LocalReport lr = new LocalReport();
        //    string path = Path.Combine(Server.MapPath("~/Report"), "Shipments.rdlc");
        //    if (System.IO.File.Exists(path)) {
        //        lr.ReportPath = path;
        //    }
        //    else {
        //        RedirectToAction("Index");
        //    }
        //    List<TransportedMatrialModel> training = new List<TransportedMatrialModel>();
        //    HttpResponseMessage res = ApiCall.getFromApi("api/TransportedMatriels");
        //    TransportedMatrialModel[] dtt = null;
        //    if (res != null)
        //    {
        //        var d = res.Content.ReadAsStringAsync().Result;
        //        dtt = Newtonsoft.Json.JsonConvert.DeserializeObject<TransportedMatrialModel[]>(d);
        //        LocalReport localreport = new LocalReport();
        //        localreport.ReportPath = path;
        //        ReportDataSource reportsource = new ReportDataSource();
        //        reportsource.Name = "DataSet1";
        //        reportsource.Value = dtt;
        //        localreport.DataSources.Add(reportsource);
        //        string reptype = ReportType;
        //        string encoding, mimetype, FileNameExtention;
        //        if (ReportType == "PDF")
        //        {
        //            FileNameExtention = "pdf";
        //        }
        //        else if (ReportType == "Image")
        //        {
        //            FileNameExtention = "jpg";
        //        }
        //        else if (ReportType == "Word")
        //        { FileNameExtention = "docx"; }
        //        else { FileNameExtention = "xlsx"; }
        //        string[] streams;
        //        Warning[] warning;
        //        byte[] renderbyte;
        //        renderbyte = localreport.Render(ReportType, "", out mimetype, out encoding, 
        //            out FileNameExtention, out streams, out warning);
        //        Response.AddHeader("content-disposition", "attachment;filename=shipments_report" 
        //            + DateTime.Now + "." + FileNameExtention);
        //        return File(renderbyte, FileNameExtention);
        //    }
        //    return View();
        //}

    }
}