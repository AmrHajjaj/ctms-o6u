﻿using CTMS___O6U.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace CTMS___O6U.Controllers
{
    public class companiesController : Controller
    {
        // GET: companies
        public ActionResult Index(int? id)
        {
            try
            {
                ViewBag.RequestResult = TempData["RequestResult"].ToString();
            }
            catch (Exception) { }

            List<CompanyModel> company = new List<CompanyModel>();

            if (id == null)
            {
                HttpResponseMessage res = ApiCall.getFromApi("api/companies");

                if (res != null)
                {
                    company = res.Content.ReadAsAsync<List<CompanyModel>>().Result;
                    var ret = company.Where(e => e.Archive == false);
                    return View(ret);
                }
            }
            else
            {
                HttpResponseMessage res = ApiCall.getFromApi("api/companies/" + id);
                if (res != null)
                {
                    company.Add(res.Content.ReadAsAsync<CompanyModel>().Result);
                }
            }
            return View(company);
        }

        [HttpPost]
        public ActionResult Index(string statue)
        {

            TempData["RequestResult"] = statue;
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create([Bind(Include = "id,name,mail,address,phone,fax,postal_code")] CompanyModel company)
        {
            company.Archive = false;
            int res = ApiCall.getFromApi("api/companies/", company);
            switch (res)
            {
                case 201:
                    return Index("Record Added Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Edit(int id)
        {
            HttpResponseMessage companydata = ApiCall.getFromApi("api/companies/" + id);
            if (companydata == null)
                return HttpNotFound();

            List<CompanyModel> companys = new List<CompanyModel>();
            companys.Add(companydata.Content.ReadAsAsync<CompanyModel>().Result);
            CompanyModel company = companys[0];
            return View(company);
        }

        public ActionResult Save(int id, CompanyModel company)
        {
            int res = ApiCall.getFromApi("api/companies/" + id, company);
            switch (res)
            {
                case 201:
                    return Index("Record Saved Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Delete(int id)
        {
            int res = ApiCall.deleteFromApi("api/companies/" + id);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted !");
                default:
                    break;
            }

            return Index("Something went worng");
        }

        public ActionResult Cars(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/companies/" + id);
            CompanyModel company = new CompanyModel();
            if (res != null)
            {
                company = res.Content.ReadAsAsync<CompanyModel>().Result;
            }

            return View(company);
        }

        public ActionResult Drivers(int id)
        {

            HttpResponseMessage res = ApiCall.getFromApi("api/companies/" + id);
            CompanyModel company = new CompanyModel();
            if (res != null)
            {
                company = res.Content.ReadAsAsync<CompanyModel>().Result;
            }

            return View(company);
        }

        public ActionResult Details(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/companies/" + id);
            CompanyModel com = new CompanyModel();
            if (res != null)
            {
                com = res.Content.ReadAsAsync<CompanyModel>().Result;
            }

            return View(com);
        }

        public ActionResult DeleteMaterial(int id)
        {

            int res = ApiCall.deleteFromApi("api/ComapnyMaterials/" + id);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted!");
                default:
                    break;
            }

            return Index("Something went worng");
        }

        public ActionResult Assign(int id)
        {
            ViewBag.id = id;
            return View();
        }

        [HttpPost]
        public ActionResult Assign(string dataName)
        {
            try
            {
                string cleanedData = Regex.Replace(dataName, @"[^0-9a-zA-Z,]", "");
                string[] dataVal = cleanedData.Split(',');
                int[] myInts = Array.ConvertAll(dataVal, s => int.Parse(s));
                for (int i = 0; i < myInts.Length; i++)
                {
                    CompanyMaterialModel addData = new CompanyMaterialModel();
                    addData.matrial_id = myInts[i]; /*(int)Char.GetNumericValue(dataName[i]);*/
                    addData.company_id = Convert.ToInt32(RouteData.Values["id"]);

                    int res = ApiCall.getFromApi("api/ComapnyMaterials", addData);

                }
                return Index("Driver Assigned");
            }
            catch (HttpException e)
            {
                return Index("Something went worng");
            }

        }


        public ActionResult Shipment(int id)
        {

            List<TransportedMatrialModel> company = new List<TransportedMatrialModel>();
            HttpResponseMessage res = ApiCall.getFromApi("api/TransportedMatriels");

            if (res != null)
            {
                company = res.Content.ReadAsAsync<List<TransportedMatrialModel>>().Result;
                var ret = company.Where(e => e.Archive == false);
                //ret = company.Join(inner: e=>e.id,)
                //return View(ret);
            }

            return View();
        }

    }
}