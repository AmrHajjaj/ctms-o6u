﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CTMS___O6U.Models;
using System.Net.Http;
using PagedList;
using PagedList.Mvc;

namespace CTMS___O6U.Controllers
{
    public class carsController : Controller
    {
        // GET: cars
        public ActionResult Index(int? id)
        {
            //try
            //{
            //    ViewBag.RequestResult = TempData["RequestResult"].ToString();
            //}
            //catch (Exception) { }
            //List<CarModel> cars = new List<CarModel>();
            //HttpResponseMessage res = ApiCall.getFromApi("api/cars?page");
            //if (res != null)
            //{
            //    cars = res.Content.ReadAsAsync<List<CarModel>>().Result;
            //}
            //return View(cars);

            try
            {
                ViewBag.RequestResult = TempData["RequestResult"].ToString();
            }
            catch (Exception) { }

            List<CarModel> cars = new List<CarModel>();

            if (id == null)
            {
                HttpResponseMessage res = ApiCall.getFromApi("api/cars");

                if (res != null)
                {
                    cars = res.Content.ReadAsAsync<List<CarModel>>().Result;
                    var ret = cars.Where(e => e.Archive == false);
                    return View(ret);
                }
            }
            else
            {
                HttpResponseMessage res = ApiCall.getFromApi("api/cars/" + id);
                if (res != null)
                {
                    cars.Add(res.Content.ReadAsAsync<CarModel>().Result);
                }
            }
            return View(cars);
        }

        [HttpPost]
        public ActionResult Index(string statue)
        {

            TempData["RequestResult"] = statue;
            return RedirectToAction("Index");
        }

        public ActionResult Create()
        {
            return View();
        }
    

        [HttpPost]
        public ActionResult Create([Bind(Include = "id,plate_number,color,Capcity,width,length,weight,satisfied,model,company_id")] CarModel car)
        {
            car.Archive = false;
            int res = ApiCall.getFromApi("api/cars", car);
            switch (res)
            {
                case 201:
                    return Index("Record Added Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Edit(int id)
        {
            ViewBag.id = id;
            HttpResponseMessage cardata = ApiCall.getFromApi("api/cars/" + id);
            if (cardata == null)
                return HttpNotFound();

            List<CarModel> cars = new List<CarModel>();
            cars.Add(cardata.Content.ReadAsAsync<CarModel>().Result);
            CarModel car = cars[0];
            return View(car);
        }

        public ActionResult Save(int id, CarModel car)
        {
            int res = ApiCall.getFromApi("api/cars/" + id, car);
            switch (res)
            {
                case 201:
                    return Index("Record Saved Successfully");
                default:
                    break;
            }
            return Index("Something went worng");
        }

        public ActionResult Delete(int id)
        {
            int res = ApiCall.deleteFromApi("api/cars/"+id);
            switch (res)
            {
                case 200:
                    return Index("Record Deleted !");
                default:
                    break;
            }

            return Index("Something went worng");
        }

        public ActionResult Details(int id)
        {
            HttpResponseMessage res = ApiCall.getFromApi("api/cars/" + id);
            CarModel car = new CarModel();
            if (res != null)
            {
                car = res.Content.ReadAsAsync<CarModel>().Result;
            }

            return View(car);
        }

    }
}