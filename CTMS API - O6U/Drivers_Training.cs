//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CTMS_API___O6U
{
    using System;
    using System.Collections.Generic;
    
    public partial class Drivers_Training
    {
        public int id { get; set; }
        public int Driver_id { get; set; }
        public int Training_id { get; set; }
        public bool Archive { get; set; }
        public System.DateTime StartingDate { get; set; }
        public string Notes { get; set; }
    
        public virtual driver driver { get; set; }
        public virtual Training Training1 { get; set; }
    }
}
