﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CTMS_API___O6U
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class CtmsDbContext : DbContext
    {
        public CtmsDbContext()
            : base("name=CtmsDbContext")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<car> cars { get; set; }
        public virtual DbSet<company> companies { get; set; }
        public virtual DbSet<car_transport_driver> car_transport_driver { get; set; }
        public virtual DbSet<driver> drivers { get; set; }
        public virtual DbSet<Drivers_Training> Drivers_Training { get; set; }
        public virtual DbSet<DummyMaterial> DummyMaterials { get; set; }
        public virtual DbSet<Training> Trainings { get; set; }
        public virtual DbSet<Comapny_Materials> Comapny_Materials { get; set; }
        public virtual DbSet<transported_matriels> transported_matriels { get; set; }
    
        //public virtual ObjectResult<spGetCars_Result1> spGetCars(Nullable<int> startIndex, Nullable<int> maximumRows)
        //{
        //    var startIndexParameter = startIndex.HasValue ?
        //        new ObjectParameter("StartIndex", startIndex) :
        //        new ObjectParameter("StartIndex", typeof(int));
    
        //    var maximumRowsParameter = maximumRows.HasValue ?
        //        new ObjectParameter("MaximumRows", maximumRows) :
        //        new ObjectParameter("MaximumRows", typeof(int));
    
        //    return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<spGetCars_Result1>("spGetCars", startIndexParameter, maximumRowsParameter);
        //}
    }
}
