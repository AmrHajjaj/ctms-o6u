﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;

namespace CTMS_API___O6U
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // Web API configuration and services
            var enableCors = new EnableCorsAttribute("*", 
                "Origin, Content-Type, Accept", 
                "GET, PUT, POST, DELETE, OPTIONS");

            config.EnableCors(enableCors);

            var setting = config.Formatters.JsonFormatter.SerializerSettings;
            setting.ContractResolver = new CamelCasePropertyNamesContractResolver();
            setting.Formatting = Formatting.Indented;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
           
            config.Routes.MapHttpRoute("DefaultApiWithPage", "Api/{controller}/{page}",
                new { page = RouteParameter.Optional }, new { page = @"\d+" });

        }
    }
}
