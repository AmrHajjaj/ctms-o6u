﻿using System.Web;
using System.Web.Mvc;

namespace CTMS_API___O6U
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
