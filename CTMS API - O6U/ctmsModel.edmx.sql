
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/06/2018 19:28:19
-- Generated from EDMX file: C:\Users\Amr\Documents\Visual Studio 2017\Projects\ctms-o6u\CTMS API - O6U\ctmsModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ctms];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[ctmsModelStoreContainer].[FK_car_transport_cars]', 'F') IS NOT NULL
    ALTER TABLE [ctmsModelStoreContainer].[car_transport_driver] DROP CONSTRAINT [FK_car_transport_cars];
GO
IF OBJECT_ID(N'[ctmsModelStoreContainer].[FK_car_transport_drivers]', 'F') IS NOT NULL
    ALTER TABLE [ctmsModelStoreContainer].[car_transport_driver] DROP CONSTRAINT [FK_car_transport_drivers];
GO
IF OBJECT_ID(N'[ctmsModelStoreContainer].[FK_car_transport_transported_matriels]', 'F') IS NOT NULL
    ALTER TABLE [ctmsModelStoreContainer].[car_transport_driver] DROP CONSTRAINT [FK_car_transport_transported_matriels];
GO
IF OBJECT_ID(N'[dbo].[FK_cars_companies]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[cars] DROP CONSTRAINT [FK_cars_companies];
GO
IF OBJECT_ID(N'[dbo].[FK_drivers_companies]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[drivers] DROP CONSTRAINT [FK_drivers_companies];
GO
IF OBJECT_ID(N'[dbo].[FK_Drivers_Training_drivers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Drivers_Training] DROP CONSTRAINT [FK_Drivers_Training_drivers];
GO
IF OBJECT_ID(N'[dbo].[FK_Drivers_Training_Training]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Drivers_Training] DROP CONSTRAINT [FK_Drivers_Training_Training];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[cars]', 'U') IS NOT NULL
    DROP TABLE [dbo].[cars];
GO
IF OBJECT_ID(N'[dbo].[companies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[companies];
GO
IF OBJECT_ID(N'[dbo].[drivers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[drivers];
GO
IF OBJECT_ID(N'[dbo].[Drivers_Training]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Drivers_Training];
GO
IF OBJECT_ID(N'[dbo].[Training]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Training];
GO
IF OBJECT_ID(N'[dbo].[transported_matriels]', 'U') IS NOT NULL
    DROP TABLE [dbo].[transported_matriels];
GO
IF OBJECT_ID(N'[ctmsModelStoreContainer].[car_transport_driver]', 'U') IS NOT NULL
    DROP TABLE [ctmsModelStoreContainer].[car_transport_driver];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'cars'
CREATE TABLE [dbo].[cars] (
    [id] int IDENTITY(1,1) NOT NULL,
    [plate_number] nvarchar(8)  NOT NULL,
    [color] nvarchar(50)  NOT NULL,
    [width] int  NOT NULL,
    [length] int  NOT NULL,
    [weight] int  NOT NULL,
    [satisfied] bit  NOT NULL,
    [model] nvarchar(50)  NOT NULL,
    [company_id] int  NOT NULL
);
GO

-- Creating table 'companies'
CREATE TABLE [dbo].[companies] (
    [id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(50)  NOT NULL,
    [mail] varchar(50)  NOT NULL,
    [address] nvarchar(50)  NOT NULL,
    [phone] nvarchar(20)  NOT NULL,
    [fax] nvarchar(50)  NOT NULL,
    [postal_code] nvarchar(20)  NOT NULL,
    [matriel] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'transported_matriels'
CREATE TABLE [dbo].[transported_matriels] (
    [id] int IDENTITY(1,1) NOT NULL,
    [source] nvarchar(50)  NOT NULL,
    [destination] nvarchar(50)  NOT NULL,
    [time_stamp] datetime  NOT NULL,
    [Quantity] int  NOT NULL
);
GO

-- Creating table 'car_transport_driver'
CREATE TABLE [dbo].[car_transport_driver] (
    [car_id] int  NOT NULL,
    [transported_id] int  NOT NULL,
    [driver_id] int  NOT NULL
);
GO

-- Creating table 'drivers'
CREATE TABLE [dbo].[drivers] (
    [id] int IDENTITY(1,1) NOT NULL,
    [ssn] nvarchar(14)  NOT NULL,
    [phone] nvarchar(20)  NOT NULL,
    [name] nvarchar(50)  NOT NULL,
    [address] nvarchar(50)  NOT NULL,
    [birth_date] datetime  NOT NULL,
    [license_degree] char(1)  NOT NULL,
    [Archive] bit  NOT NULL,
    [companys_id] int  NOT NULL
);
GO

-- Creating table 'Drivers_Training'
CREATE TABLE [dbo].[Drivers_Training] (
    [id] int IDENTITY(1,1) NOT NULL,
    [Driver_id] int  NOT NULL,
    [Training_id] int  NOT NULL,
    [Archive] bit  NOT NULL,
    [StartingDate] datetime  NOT NULL
);
GO

-- Creating table 'Trainings'
CREATE TABLE [dbo].[Trainings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Duration] int  NOT NULL,
    [Archive] bit  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id] in table 'cars'
ALTER TABLE [dbo].[cars]
ADD CONSTRAINT [PK_cars]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'companies'
ALTER TABLE [dbo].[companies]
ADD CONSTRAINT [PK_companies]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'transported_matriels'
ALTER TABLE [dbo].[transported_matriels]
ADD CONSTRAINT [PK_transported_matriels]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [car_id], [transported_id], [driver_id] in table 'car_transport_driver'
ALTER TABLE [dbo].[car_transport_driver]
ADD CONSTRAINT [PK_car_transport_driver]
    PRIMARY KEY CLUSTERED ([car_id], [transported_id], [driver_id] ASC);
GO

-- Creating primary key on [id] in table 'drivers'
ALTER TABLE [dbo].[drivers]
ADD CONSTRAINT [PK_drivers]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [id] in table 'Drivers_Training'
ALTER TABLE [dbo].[Drivers_Training]
ADD CONSTRAINT [PK_Drivers_Training]
    PRIMARY KEY CLUSTERED ([id] ASC);
GO

-- Creating primary key on [Id] in table 'Trainings'
ALTER TABLE [dbo].[Trainings]
ADD CONSTRAINT [PK_Trainings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [car_id] in table 'car_transport_driver'
ALTER TABLE [dbo].[car_transport_driver]
ADD CONSTRAINT [FK_car_transport_cars]
    FOREIGN KEY ([car_id])
    REFERENCES [dbo].[cars]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [company_id] in table 'cars'
ALTER TABLE [dbo].[cars]
ADD CONSTRAINT [FK_cars_companies]
    FOREIGN KEY ([company_id])
    REFERENCES [dbo].[companies]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_cars_companies'
CREATE INDEX [IX_FK_cars_companies]
ON [dbo].[cars]
    ([company_id]);
GO

-- Creating foreign key on [transported_id] in table 'car_transport_driver'
ALTER TABLE [dbo].[car_transport_driver]
ADD CONSTRAINT [FK_car_transport_transported_matriels]
    FOREIGN KEY ([transported_id])
    REFERENCES [dbo].[transported_matriels]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_car_transport_transported_matriels'
CREATE INDEX [IX_FK_car_transport_transported_matriels]
ON [dbo].[car_transport_driver]
    ([transported_id]);
GO

-- Creating foreign key on [companys_id] in table 'drivers'
ALTER TABLE [dbo].[drivers]
ADD CONSTRAINT [FK_drivers_companies]
    FOREIGN KEY ([companys_id])
    REFERENCES [dbo].[companies]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_drivers_companies'
CREATE INDEX [IX_FK_drivers_companies]
ON [dbo].[drivers]
    ([companys_id]);
GO

-- Creating foreign key on [driver_id] in table 'car_transport_driver'
ALTER TABLE [dbo].[car_transport_driver]
ADD CONSTRAINT [FK_car_transport_drivers]
    FOREIGN KEY ([driver_id])
    REFERENCES [dbo].[drivers]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_car_transport_drivers'
CREATE INDEX [IX_FK_car_transport_drivers]
ON [dbo].[car_transport_driver]
    ([driver_id]);
GO

-- Creating foreign key on [Driver_id] in table 'Drivers_Training'
ALTER TABLE [dbo].[Drivers_Training]
ADD CONSTRAINT [FK_Drivers_Training_drivers]
    FOREIGN KEY ([Driver_id])
    REFERENCES [dbo].[drivers]
        ([id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Drivers_Training_drivers'
CREATE INDEX [IX_FK_Drivers_Training_drivers]
ON [dbo].[Drivers_Training]
    ([Driver_id]);
GO

-- Creating foreign key on [Training_id] in table 'Drivers_Training'
ALTER TABLE [dbo].[Drivers_Training]
ADD CONSTRAINT [FK_Drivers_Training_Training]
    FOREIGN KEY ([Training_id])
    REFERENCES [dbo].[Trainings]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Drivers_Training_Training'
CREATE INDEX [IX_FK_Drivers_Training_Training]
ON [dbo].[Drivers_Training]
    ([Training_id]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------