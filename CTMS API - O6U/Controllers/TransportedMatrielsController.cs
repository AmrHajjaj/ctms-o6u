﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.Data.Entity.Validation;

namespace CTMS_API___O6U.Controllers
{
    public class TransportedMatrielsController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/TransportedMatriels
        public IQueryable<transported_matriels> Gettransported_matriels()
        {
            return db.transported_matriels.Where(e => e.Archive == false);
        }

        // GET: api/TransportedMatriels/5
        [ResponseType(typeof(transported_matriels))]
        public IHttpActionResult Gettransported_matriels(int id)
        {
            transported_matriels transported_matriels = db.transported_matriels.Find(id);
            if (transported_matriels == null)
            {
                return NotFound();
            }

            return Ok(transported_matriels);
        }

        // PUT: api/TransportedMatriels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Puttransported_matriels(int id, transported_matriels transported_matriels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != transported_matriels.id)
            {
                return BadRequest();
            }

            db.Entry(transported_matriels).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!transported_matrielsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TransportedMatriels
        [ResponseType(typeof(transported_matriels))]
        public IHttpActionResult Posttransported_matriels(transported_matriels transported_matriels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(transported_matriels.id == 0)
            {
                try
                {
                    db.transported_matriels.Add(transported_matriels);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                var transportedInDb = db.transported_matriels.Single(c => c.id == transported_matriels.id);

                transportedInDb.source = transported_matriels.source;
                transportedInDb.destination = transported_matriels.destination;
                transportedInDb.time_stamp = transported_matriels.time_stamp;
                transportedInDb.Quantity = transported_matriels.Quantity;

                db.SaveChanges();
            }

            

            return CreatedAtRoute("DefaultApi", new { id = transported_matriels.id }, transported_matriels);
        }

        // DELETE: api/TransportedMatriels/5
        [ResponseType(typeof(transported_matriels))]
        public IHttpActionResult Deletetransported_matriels(int id)
        {
            transported_matriels transported_matriels = db.transported_matriels.Find(id);
            if (transported_matriels == null)
            {
                return NotFound();
            }

            db.transported_matriels.Remove(transported_matriels);
            db.SaveChanges();

            return Ok(transported_matriels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool transported_matrielsExists(int id)
        {
            return db.transported_matriels.Count(e => e.id == id) > 0;
        }
    }
}