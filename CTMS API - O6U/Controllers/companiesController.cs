﻿    using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace CTMS_API___O6U.Controllers
{
    public class companiesController : ApiController
    {

        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/companies
        public IHttpActionResult Getcompanies()
        {
            return Ok(db.companies.ToList());
        }

        // GET: api/companies/5
        [ResponseType(typeof(company))]
        public IHttpActionResult Getcompany(int id)
        {
            company company = db.companies.Find(id);
            if (company == null)
            {
                return NotFound();
            }

            return Ok(company);
        }

        // PUT: api/companies/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult Putcompany(int id, company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != company.id)
            {
                return BadRequest();
            }

            db.Entry(company).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!companyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/companies
        [ResponseType(typeof(company))]
        [HttpPost]
        public IHttpActionResult Postcompany(company company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (company.id == 0)
            {
                try
                {
                    db.companies.Add(company);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                var companyInDb = db.companies.Single(c => c.id == company.id);

                companyInDb.name = company.name;
                companyInDb.mail = company.mail;
                companyInDb.address = company.address;
                companyInDb.phone = company.phone;
                companyInDb.fax = company.fax;
                companyInDb.postal_code = company.postal_code;
                //companyInDb.matriel = company.matriel;

                db.SaveChanges();
            }

                return CreatedAtRoute("DefaultApi", new { id = company.id }, company);
        }

        // DELETE: api/companies/5
        [HttpDelete]
        [ResponseType(typeof(company))]
        public IHttpActionResult Deletecompany(int id)
        {
            var driverList = db.drivers.Where(e => e.companys_id == id);
            DriversController d = new DriversController();
            foreach (driver dr in driverList)
                d.Deletedriver(dr.id);
            GC.Collect();

            var carList = db.cars.Where(e => e.company_id == id);
            carsController c = new carsController();
            foreach (car ca in carList)
                c.Deletecar(ca.id);
            GC.Collect();

            var sql = @"UPDATE dbo.Comapny_Materials SET Archive=@Archive
                        WHERE company_id = @id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@id", id));

            sql = @"UPDATE dbo.companies SET Archive=@Archive
                        WHERE id = @id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@id", id));

            //company company = db.companies.Find(id);
            //if (company == null)
            //{
            //    return NotFound();
            //}

            //db.companies.Remove(company);
            db.SaveChanges();

            return Ok(/*company*/);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool companyExists(int id)
        {
            return db.companies.Count(e => e.id == id) > 0;
        }
    }
}