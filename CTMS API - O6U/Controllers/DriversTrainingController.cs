﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace CTMS_API___O6U.Controllers
{
    public class DriversTrainingController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/DriversTraining
        public IQueryable<Drivers_Training> GetDrivers_Training()
        {
            return db.Drivers_Training;
        }

        // GET: api/DriversTraining/5
        [ResponseType(typeof(Drivers_Training))]
        public IHttpActionResult GetDrivers_Training(int id)
        {
            Drivers_Training drivers_Training = db.Drivers_Training.Find(id);
            if (drivers_Training == null)
            {
                return NotFound();
            }

            return Ok(drivers_Training);
        }

        // PUT: api/DriversTraining/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PutDrivers_Training(int id, Drivers_Training drivers_Training)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != drivers_Training.id)
            {
                return BadRequest();
            }

            db.Entry(drivers_Training).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Drivers_TrainingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DriversTraining
        [ResponseType(typeof(Drivers_Training))]
        [HttpPost]
        public IHttpActionResult PostDrivers_Training(Drivers_Training drivers_Training)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (drivers_Training.Training_id == 0)
                return BadRequest();

            if (TrainingDriverExists(drivers_Training.Driver_id ,drivers_Training.Training_id))
                return Conflict();


            db.Drivers_Training.Add(drivers_Training);
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {}
            return CreatedAtRoute("DefaultApi", new { id = drivers_Training.id }, drivers_Training);
        }

        // DELETE: api/DriversTraining/5
        [ResponseType(typeof(Drivers_Training))]
        [HttpDelete]
        public IHttpActionResult DeleteDrivers_Training([FromUri] int[] ids)
        {
            //var drivers_Training = db.Drivers_Training.SqlQuery("DELETE from dbo.Drivers_Training " +
            //    " WHERE Driver_id= " + ids[0] + " and Training_id= " + ids[1] + " ;");
            //Where(e => e.Driver_id == ids[0] && e.Training_id == ids[1])
            //.Select(e => e.id);

            //var sql = @"DELETE FROM dbo.Drivers_Training
            //            WHERE Driver_id = @Driver_id ANd Training_id = @Training_id";
            var sql = @"UPDATE dbo.Drivers_Training SET Archive=@Archive
                        WHERE Driver_id = @Driver_id ANd Training_id = @Training_id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive" , true),
                new SqlParameter("@Driver_id", ids[0]),
                new SqlParameter("@Training_id", ids[1]));
            

            //if (drivers_Training == null)
            //{
            //    return NotFound();
            //}

            //db.Drivers_Training.Remove(drivers_Training);
            db.SaveChanges();

            return Ok(/*drivers_Training*/);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Drivers_TrainingExists(int id)
        {
            return db.Drivers_Training.Count(e => e.id == id) > 0;
        }

        private bool DriverExists(int id)
        {
            return db.Drivers_Training.Count(e => e.Driver_id == id) > 0;
        }

        private bool TrainingExists(int id)
        {
            return db.Drivers_Training.Count(e => e.Training_id == id) > 0;
        }

        private bool TrainingDriverExists(int trainId, int driverID)
        {
            return db.Drivers_Training.Count(e => e.Training_id == trainId && e.Driver_id == driverID) > 0;
        }

    }
}