﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;

namespace CTMS_API___O6U.Controllers
{
    public class DummyMaterialsController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/DummyMaterials
        public IQueryable<DummyMaterial> GetDummyMaterials()
        {
            return db.DummyMaterials;
        }

        // GET: api/DummyMaterials/5
        [ResponseType(typeof(DummyMaterial))]
        public IHttpActionResult GetDummyMaterial(int id)
        {
            DummyMaterial dummyMaterial = db.DummyMaterials.Find(id);
            if (dummyMaterial == null)
            {
                return NotFound();
            }

            return Ok(dummyMaterial);
        }

        
    }
}