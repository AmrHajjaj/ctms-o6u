﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.Data.SqlClient;

namespace CTMS_API___O6U.Controllers
{
    public class CarTransportDriverController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/CarTransportDriver
        public IQueryable<car_transport_driver> Getcar_transport_driver()
        {
            return db.car_transport_driver.Where(e => e.Archive == false);
        }

        // GET: api/CarTransportDriver/5
        [ResponseType(typeof(car_transport_driver))]
        public IHttpActionResult Getcar_transport_driver(int id)
        {
            car_transport_driver car_transport_driver = db.car_transport_driver.Find(id);
            if (car_transport_driver == null)
            {
                return NotFound();
            }

            return Ok(car_transport_driver);
        }

        // PUT: api/CarTransportDriver/5
        [ResponseType(typeof(void))]
        public IHttpActionResult Putcar_transport_driver(int id, car_transport_driver car_transport_driver)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != car_transport_driver.car_id)
            {
                return BadRequest();
            }

            db.Entry(car_transport_driver).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!car_transport_driverExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CarTransportDriver
        [ResponseType(typeof(car_transport_driver))]
        public IHttpActionResult Postcar_transport_driver(car_transport_driver car_transport_driver)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.car_transport_driver.Add(car_transport_driver);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (car_transport_driverExists(car_transport_driver.car_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = car_transport_driver.car_id }, car_transport_driver);
        }

        // DELETE: api/CarTransportDriver/5
        [ResponseType(typeof(car_transport_driver))]
        [HttpDelete]
        public IHttpActionResult Deletecar_transport_driver(int id)
        {
            var sql = @"UPDATE dbo.car_transport_driver SET Archive=@Archive
                        WHERE id = @id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@id", id)
                );

            //car_transport_driver car_transport_driver = db.car_transport_driver.Find(id);
            //if (car_transport_driver == null)
            //{
            //    return NotFound();
            //}

            //db.car_transport_driver.Remove(car_transport_driver);
            db.SaveChanges();

            return Ok();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool car_transport_driverExists(int id)
        {
            return db.car_transport_driver.Count(e => e.car_id == id) > 0;
        }
    }
}