﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.Data.Entity.Validation;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;

namespace CTMS_API___O6U.Controllers
{
    [Table("drivers")]
    public class DriversController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/Drivers
        public IHttpActionResult Getdrivers()
        {
            //if (id == null)
            return Ok(db.drivers.ToList());
            //else
            //    return Ok(db.drivers.Where(e => e.Archive == id).ToList());
        }

        // GET: api/Drivers/5
        [ResponseType(typeof(driver))]
        public IHttpActionResult Getdriver(int id)
        {
            driver driver = db.drivers.Find(id);
            if (driver == null)
            {
                return NotFound();
            }

            return Ok(driver);
        }

        //GET: api/Drivers/Bycompany/5
        [Route("api/drivers/bycompany/{id}")]
        [HttpGet]
        public IHttpActionResult bycompany(int id)
        {
            return Ok(db.drivers.Where(p => p.companys_id == id).ToList());
        }

        // PUT: api/Drivers/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult Putdriver(int id, driver driver)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != driver.id)
            {
                return BadRequest();
            }

            db.Entry(driver).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!driverExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Drivers
        [ResponseType(typeof(driver))]
        [HttpPost]
        public IHttpActionResult Postdriver(driver driver)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (driver.id == 0)
            {
                try
                {

                    db.drivers.Add(driver);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                var driverInDb = db.drivers.Single(c => c.id == driver.id);

                driverInDb.ssn = driver.ssn;
                driverInDb.phone = driver.phone;
                driverInDb.name = driver.name;
                driverInDb.address = driver.address;
                driverInDb.birth_date = driver.birth_date;
                driverInDb.license_degree = driver.license_degree;
                driverInDb.companys_id = driver.companys_id;

                db.SaveChanges();
            }

            return CreatedAtRoute("DefaultApi", new { id = driver.id }, driver);
        }

        // DELETE: api/Drivers/5
        [ResponseType(typeof(driver))]
        [HttpDelete]
        public IHttpActionResult Deletedriver(int id)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //var d = db.drivers.Find(id);
            var driver = new driver {id=id ,Archive = true };
            var trainingList = db.Drivers_Training.Where(s => s.Driver_id == id).ToList();

            var sql = @"UPDATE dbo.drivers SET Archive=@Archive
                        WHERE id = @Driver_id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@Driver_id", id));

            sql = @"UPDATE dbo.Drivers_Training SET Archive=@Archive
                        WHERE Driver_id = @Driver_id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@Driver_id", id));
            db.SaveChanges();
            //if (trainingList != null || trainingList.Count != 0)
            //{
            //    foreach (var t in trainingList)
            //    {
            //        var dT = new Drivers_Training { id = t.id, Archive = true };
            //        db.Entry(dT).Property(dt=>dt.Archive).IsModified = true;
            //    }
            //}

            //db.drivers.Attach(driver);
            //db.Entry(driver).Property(d => d.Archive).IsModified = true;
            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (System.Data.Entity.Validation.DbEntityValidationException dbEx)
            //{
            //    Exception raise = dbEx;
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            string message = string.Format("{0}:{1}",
            //                validationErrors.Entry.Entity.ToString(),
            //                validationError.ErrorMessage);
            //            // raise a new exception nesting
            //            // the current instance as InnerException
            //            raise = new InvalidOperationException(message, raise);
            //        }
            //    }
            //    throw raise;
            //}
            //return StatusCode(HttpStatusCode.NoContent);

            //driver driver = db.drivers.Find(id);
            //var trainingList = db.Drivers_Training.Where(s => s.Driver_id == id).ToList();
            //if (driver == null)
            //{
            //    return NotFound();
            //}
            //if (trainingList != null)
            //{
            //    foreach(var t in trainingList)
            //        db.Drivers_Training.Remove(t);
            //}
            //db.drivers.Remove(driver);
            //db.SaveChanges();

            return Ok(driver);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool driverExists(int id)
        {
            return db.drivers.Count(e => e.id == id) > 0;
        }
    }
}