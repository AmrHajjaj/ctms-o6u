﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace CTMS_API___O6U.Controllers
{
    [Table("Cars")]
    public class carsController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/cars
        [HttpGet]
        public IHttpActionResult Getcars()
        {
            return Ok(db.cars.ToList());
        }


        //[HttpGet]
        //public IHttpActionResult Getcars(int? page)
        //{
        //    return Ok(db.spGetCars(page??1, 3));
        //}

        // GET: api/cars/5
        [ResponseType(typeof(car))] 
        public IHttpActionResult Getcar(int id)
        {
            car car = db.cars.Find(id);
            if (car == null)
            {
                return NotFound();
            }

            return Ok(car);
        }

        //[ResponseType(typeof(car))]
        //public IHttpActionResult Get(int? page)
        //{

        //    return Ok(db.spGetCars(page ?? 1 ,1));
        //}

        //GET: api/cars/Bycompany/5
        [Route("api/cars/bycompany/{id}")]
        [HttpGet]
        public IHttpActionResult bycompany(int id)
        {
            return Ok(db.cars.Where(p => p.company_id == id).ToList());
        }

        // PUT: api/cars/5
        [ResponseType(typeof(car))]
        [HttpPut]
        public IHttpActionResult Putcar(int id, car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != car.id)
            {
                return BadRequest();
            }

            db.Entry(car).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!carExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/cars
        [ResponseType(typeof(car))]
        [HttpPost]
        public IHttpActionResult Postcar(car car)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(car.id == 0)
            {
                try
                {

                    db.cars.Add(car);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                var carInDb = db.cars.Single(c => c.id == car.id);

                carInDb.plate_number = car.plate_number;
                carInDb.color = car.color;
                carInDb.Capcity = car.Capcity;
                carInDb.width = car.width;
                carInDb.length = car.length;
                carInDb.weight = car.weight;
                carInDb.satisfied = car.satisfied;
                carInDb.model = car.model;
                carInDb.company_id = car.company_id;

                db.SaveChanges();
            }
            

            return CreatedAtRoute("DefaultApi", new { id = car.id }, car);
        }

        // DELETE: api/cars/5
        [ResponseType(typeof(car))]
        [HttpDelete]
        public IHttpActionResult Deletecar(int id)
        {
            //car car = db.cars.Find(id);
            //if (car == null)
            //{
            //    return NotFound();
            //}
            var sql = @"UPDATE dbo.cars SET Archive=@Archive
                        WHERE id = @id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@id", id));
            //db.cars.Remove(car);
            db.SaveChanges();

            return Ok(/*car*/);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool carExists(int id)
        {
            return db.cars.Count(e => e.id == id) > 0;
        }
    }
}