﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;

namespace CTMS_API___O6U.Controllers
{
    public class ComapnyMaterialsController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/Comapny_Materials
        public IQueryable<Comapny_Materials> GetComapny_Materials()
        {
            return db.Comapny_Materials;
        }

        // GET: api/Comapny_Materials/5
        [ResponseType(typeof(Comapny_Materials))]
        public IHttpActionResult GetComapny_Materials(int id)
        {
            Comapny_Materials comapny_Materials = db.Comapny_Materials.Find(id);
            if (comapny_Materials == null)
            {
                return NotFound();
            }

            return Ok(comapny_Materials);
        }

        // PUT: api/Comapny_Materials/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutComapny_Materials(int id, Comapny_Materials comapny_Materials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != comapny_Materials.id)
            {
                return BadRequest();
            }

            db.Entry(comapny_Materials).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Comapny_MaterialsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Comapny_Materials
        [ResponseType(typeof(Comapny_Materials))]
        public IHttpActionResult PostComapny_Materials(Comapny_Materials comapny_Materials)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (comapny_Materials.matrial_id == 0)
                return BadRequest();

            if (CompanyMaterialExists(comapny_Materials.company_id, comapny_Materials.matrial_id))
                return Conflict();
            db.Comapny_Materials.Add(comapny_Materials);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = comapny_Materials.id }, comapny_Materials);
        }

        // DELETE: api/Comapny_Materials/5
        [ResponseType(typeof(Comapny_Materials))]
        public IHttpActionResult DeleteComapny_Materials(int id)
        {
            //Comapny_Materials comapny_Materials = db.Comapny_Materials.Find(id);
            //if (comapny_Materials == null)
            //{
            //    return NotFound();
            //}

            //db.Comapny_Materials.Remove(comapny_Materials);'
            var sql = @"UPDATE dbo.Comapny_Materials SET Archive=@Archive
                        WHERE id = @id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@id", id));

            db.SaveChanges();

            return Ok(/*comapny_Materials*/);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Comapny_MaterialsExists(int id)
        {
            return db.Comapny_Materials.Count(e => e.id == id) > 0;
        }

        private bool CompanyMaterialExists(int comId, int matId)
        {
            return db.Comapny_Materials.Count(e => e.company_id == comId && e.company_id == matId) > 0;
        }

    }
}