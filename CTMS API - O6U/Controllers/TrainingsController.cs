﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using CTMS_API___O6U;
using System.Data.Entity.Validation;
using System.Data.SqlClient;

namespace CTMS_API___O6U.Controllers
{
    public class TrainingsController : ApiController
    {
        private CtmsDbContext db = new CtmsDbContext();

        // GET: api/Trainings
        public IQueryable<Training> GetTrainings()
        {
            return db.Trainings;
        }

        // GET: api/Trainings/5
        [ResponseType(typeof(Training))]
        public IHttpActionResult GetTraining(int id)
        {
            Training training = db.Trainings.Find(id);
            if (training == null)
            {
                return NotFound();
            }

            return Ok(training);
        }

        // PUT: api/Trainings/5
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult PutTraining(int id, Training training)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != training.Id)
            {
                return BadRequest();
            }

            db.Entry(training).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TrainingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Trainings
        [ResponseType(typeof(Training))]
        [HttpPost]
        public IHttpActionResult PostTraining(Training training)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (training.Id == 0)
            {
                try
                {

                    db.Trainings.Add(training);
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    foreach (var validationErrors in dbEx.EntityValidationErrors)
                    {
                        foreach (var validationError in validationErrors.ValidationErrors)
                        {
                            System.Console.WriteLine("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                        }
                    }
                }
            }
            else
            {
                var trainingInDb = db.Trainings.Single(c => c.Id == training.Id);

                trainingInDb.Name = training.Name;
                trainingInDb.Duration = training.Duration;

                db.SaveChanges();
            }

            return CreatedAtRoute("DefaultApi", new { id = training.Id }, training);
        }

        // DELETE: api/Trainings/5
        [ResponseType(typeof(Training))]
        [HttpDelete]
        public IHttpActionResult DeleteTraining(int id)
        {
            var sql = @"UPDATE dbo.Training SET Archive=@Archive
                        WHERE id = @Training_id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@Training_id", id));

            sql = @"UPDATE dbo.Drivers_Training SET Archive=@Archive
                        WHERE id = @Training_id";

            db.Database.ExecuteSqlCommand(
                sql,
                new SqlParameter("@Archive", true),
                new SqlParameter("@Training_id", id));

            //Training training = db.Trainings.Find(id);
            //var driverList = db.Drivers_Training.Where(s => s.Training_id == id).ToList();

            //if (training == null)
            //{
            //    return NotFound();
            //}
            //if (driverList != null)
            //{
            //    foreach (var d in driverList)
            //        db.Drivers_Training.Remove(d);
            //}
            //db.Trainings.Remove(training);
            db.SaveChanges();

            return Ok(/*training*/);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TrainingExists(int id)
        {
            return db.Trainings.Count(e => e.Id == id) > 0;
        }
    }
}